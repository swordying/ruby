<?php
/**
 * --------------------------------
 * ## PHP 最为 soap 服务端
 * --------------------------------
 * 1. PHP 开启网页服务
 * 2. PHP 安装 soap 扩展
 * 2. 目的：使 PHP 中的类可以被 Ruby 调用
 * --------------------------------
 */
require_once './Soap.php';
require_once './Wsdl.php';
class SoapSupply
{
    public function service()
    {
        // 创建一个SOAP服务器对象
        $server = new \SoapServer(null, array('uri' => 'http://localhost:8080/soap_server.php')); ## ` $ php -S localhost:8080 -t /path `

        $class = "Soap";
        // 将服务类注册到SOAP服务器中
        $server -> setClass($class);

        // 处理 SOAP 请求
        $server -> handle();
    }

    public function touchFile()
    {
        $class_name = 'Soap';
        $file_name = 'soap';
        $wsdl = new Wsdl($class_name, $file_name);
        $result = $wsdl -> touchFile();
        echo $result ? 1 : 0;
    }
}

$soap_supply = new SoapSupply();
if(empty($_GET['handle'])){
    $soap_supply -> service();
}else{
    $soap_supply -> touchFile();
}
