#!/usr/bin/ruby
## Ruby 作为 soap 客户端
## Ruby 需要安装 savon 等模块
require 'savon'

## 当服务端没有静态生成 wsdl 文件时，需要请求接口指令生成 wsdl 文件
### 引入 open-uri 包
require 'open-uri'
## 如果有 GET 请求参数直接写在 URI 地址中 
uri = 'http://localhost:8080/soap_server.php?handle=getFile'
html_response = nil
open(uri) do |http|
    html_response = http.read
end
if html_response == '0'
    puts "Soap 服务器生成 WSDL 文件失败"
end

## 请求 Soap 服务
client = Savon.client(
    wsdl: 'http://localhost:8080/soap.wsdl',
    endpoint: 'http://localhost:8080/soap_server.php',
)

response = client.call(:index)

if response.success?
    # result = response.body
    # result = response.body[:index_response]
    result = response.body[:index_response][:return]
    # 替换method_name为实际的方法名
    puts "服务端返回的结果: #{result}"
else
    puts "调用服务端方法失败: #{response.soap_fault}"
end
