#!/usr/bin/ruby
## Ruby 解析 JSON 字符串
require 'json'
var_hash = {
    :title => 'JSON 操作',
    :description => '先 Hash 转 JSON 然后恢复',
}
### 引入 JSON
var_json = require 'json'
### 转 JSON
var_json = var_hash.to_json #JSON.stringify(var_json);
puts var_json.class # String
puts var_json
### 解析 JSON
var_hash = JSON.parse(var_json)
puts var_hash.class # Hash
puts var_hash
## 备注：需要先安装 Ruby JSON 模块 ` $ gem install json`
