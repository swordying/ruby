## Ruby TCP/IP 笔记

### 1. HTTP GET 请求
```ruby
## 引入 open-uri 包
require 'open-uri'
## 如果有 GET 请求参数直接写在 URI 地址中 
uri = 'http://www.baidu.com'
html_response = nil
open(uri) do |http|
    html_response = http.read
end
puts html_response
```
